# pyhaversion - A python module to the newest version number of Home Assistant

## Notes

This has been tested with python 3.6  
This module uses these external libraries:

- requests

## Install

```bash
pip install pyhaversion
```

## Example usage

```python
from pyhaversion import HAVersion

source = 'pip'
branch = 'stable'
image = 'default'

ha_version = HAVersion()
result = ha_version.get_version_number(source, branch, image)

#Print results:
print('HA version: ' + result)
```

## Valid options for source

- pip
- docker
- hassio

## Valid options for branch

- stable
- beta

## Optional valid options for image (hassio)

- default
- qemux86
- qemux86-64
- qemuarm
- qemuarm-64
- intel-nuc
- raspberrypi
- raspberrypi2
- raspberrypi3
- raspberrypi3-64